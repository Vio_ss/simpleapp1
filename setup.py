import subprocess

def build():
    print("Building Python calculator application...")

    # Perform any build tasks here if necessary
    # For this simple script, no specific build tasks are required

def run_calculator():
    print("Running Python calculator application...")
    subprocess.run(["python", "calc.py"])

def main():
    build()
    run_calculator()

if __name__ == "__main__":
    main()
